package com.ashatechsoln.amit.game;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.*;


public class MainActivity extends AppCompatActivity {

    Button one,two,three,clear,done,back;
    int op1,total,p=0;
    EditText disp,disp2;
    boolean b1,b2;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        one=(Button)findViewById(R.id.button1);
        two=(Button)findViewById(R.id.button2);
        three=(Button)findViewById(R.id.button3);
        clear=(Button)findViewById(R.id.button4);
        done=(Button)findViewById(R.id.button5);
        disp = (EditText) findViewById(R.id.disp);
        back= (Button)findViewById(R.id.buttonb);
        disp2 = (EditText) findViewById(R.id.disp2);

        total=0;
        p=0;
        clear.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                if(disp != null)
                {
                    disp.setText("");
                    disp2.setText("");
                }
                p=0;
                total=0;
                // TODO Auto-generated method stub

            }
        });

        one.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                disp.setText(disp.getText()+"1");

            }
        });

        two.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                disp.setText(disp.getText()+"2");

            }
        });


        three.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                disp.setText(disp.getText()+"3");

            }
        });


        back.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                if(disp != null)
                {
                    disp.setText("");

                }

            }
        });


        done.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                if(disp == null)
                {
                    disp.setText("");
                    disp2.setText("");
                }
                else {
                    op1 = Integer.parseInt(disp.getText() + "");
                    if (op1<= 3){
                        total = total + op1;
                        disp.setText("");
                        String s = Integer.toString(total);
                        disp2.setText(s);

                        p = p + 1;
                        if (total >= 21 && p % 2 == 0) {
                            Toast.makeText(MainActivity.this, "Player 1 won!!", Toast.LENGTH_LONG).show();
                            disp2.setText("Player 1");
                            //disp2.setText("");
                            p = 0;
                            total = 0;
                        } else if (total >= 21 && p % 2 == 1) {
                            Toast.makeText(MainActivity.this, "Player 2 won!!", Toast.LENGTH_LONG).show();
                            disp2.setText("Player 2");
                            //disp2.setText("");
                            p = 0;
                            total = 0;
                        }

                    }
                    else
                    {Toast.makeText(MainActivity.this, "Out of range!!", Toast.LENGTH_LONG).show();
                        disp.setText("");}
                }
            }
        });







    }
}
